enum WebEvent {
    PageLoad,
    PageUnload,
    KeyPress(char),
    Paste(String),
    Click { x: i64, y: i64 },
}

fn inspect(event: WebEvent) {
    match event {
        WebEvent::PageLoad => println!("Page Loaded"),
        WebEvent::PageUnload => println!("Page Unloaded"),
        WebEvent::KeyPress(c) => println!("pressed '{}'", c),
        WebEvent::Paste(s) => println!("Pasted '{}'", s),
        WebEvent::Click { x, y } => println!("Click at x={} y={}", x, y),
    };
}

fn main() {
    let pressed = WebEvent::KeyPress('x');
    inspect(pressed);
    let pasted = WebEvent::Paste("something".to_owned());
    inspect(pasted);
    let click = WebEvent::Click { x: 24, y: 79 };
    inspect(click);
    let load = WebEvent::PageLoad;
    inspect(load);
    let unload = WebEvent::PageUnload;
    inspect(unload);
}