use crate::Status::Rich;
use crate::Color::Black;

enum Status {
    Rich,
    Poor,
}

enum Color {
    Black = 0xFFFFFF,
    MarineBlue = 0xFFFF00,
}

fn identify(status: Status, color: Color) {
    use Status::*;
    match status {
        Poor => println!("You're poor"),
        Rich => println!("You're rich"),
    }
    match color {
        c@Color::Black => println!("roses are #{:06x}", c as i32),
        c@Color::MarineBlue => println!("roses are #{:06x}", c as i32),
    }
}

fn main() {
    identify(Rich, Black)
}