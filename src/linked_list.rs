use crate::List::*;

enum List {
    Cons(u32, Box<List>),
    Nil,
}

impl List {
    fn new() -> List {
        Nil
    }

    fn prepend(self, element: u32) -> List {
        Cons(element, Box::new(self))
    }

    fn len(&self) -> u32 {
        match *self {
            Nil => 0,
            Cons(_, ref tail) => 1 + tail.len()
        }
    }

    fn stringify(&self) -> String {
        match *self {
            Nil => format!("Nil"),
            Cons(head, ref tail) => format!("{}, {}", head, tail.stringify())
        }
    }
}

fn main() {
    let l0 = List::new();
    println!("List l.len() -> {}", l0.len());
    let l1 = l0.prepend(1);
    println!("List l.len() -> {}", l1.len());
    println!("{}", l1.stringify())
}