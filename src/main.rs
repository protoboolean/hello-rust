use std::fmt::{Display, Error, Formatter};

fn main() {
    println!("#### FORMATTING CONTAINER TYPES ####");
    formatting_container_types_such_as_vectors();
    println!("#### 1.2.3 FORMATTING ####");
    formatting_p1_2_3();
    println!("#### 2.2 TUPLES ####");
    tuples_p2_2();
    println!("#### 2.2 STRUCTURES ####");
    structures_p3_1();
}

fn formatting_container_types_such_as_vectors() {
    let mut xs = vec![1, 2, 3];
    println!("xs = {:?}", xs);
    //    println!("xs.push(4)");
    //    xs.push(4);
    //    println!("xs = {:?}", xs);
    println!("xs.len() = {}", xs.len());
    //    println!("xs[1] = {}", xs[1]);
    //    xs.pop().map(|last_item|
    //        println!("xs.pop() = {}", last_item));
    for item in xs.iter() {
        println!("> {}", item + item);
    }
    for (i, x) in xs.iter().enumerate() {
        println!("{i}> {x}", i = i, x = x);
    }
    for x in xs.iter_mut() {
        *x *= 3;
    }
    println!("Updated vector: {:?}", xs);
}

fn formatting_p1_2_3() {
    struct City {
        name: &'static str,
        // Latitude
        lat: f32,
        // Longitude
        lon: f32,
    };

    impl Display for City {
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
            let lat_c = if self.lat >= 0.0 { 'N' } else { 'S' };
            let lon_c = if self.lon >= 0.0 { 'E' } else { 'W' };

            write!(
                f,
                "{}: {:.3}º{} {:.3}º{}",
                self.name,
                self.lat.abs(),
                lat_c,
                self.lon.abs(),
                lon_c
            )
        }
    }

    // Q®: Why do I need to call `.iter()` to iterate on a collection?
    // Q®: Why do I need to use the asterisk to refer to city (`*city`)?
    let cities = [
        City {
            name: "Dublin",
            lat: 53.347778,
            lon: -6.259722,
        },
        City {
            name: "Oslo",
            lat: 59.95,
            lon: 10.75,
        },
        City {
            name: "Vancouver",
            lat: 49.25,
            lon: -123.1,
        }
    ];
    for city in cities.iter() {
        println!("{}", *city)
    }

    #[derive(Debug)]
    struct Color {
        red: u8,
        green: u8,
        blue: u8,
    }

    impl Display for Color {
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
            write!(f,
                   "RGB{{{red:>width$}, {green:>width$}, {blue:>width$}}} \
                    0x{red:02x}{green:02x}{blue:02x}",
                   red = self.red, green = self.green, blue = self.blue, width = 3
            )
        }
    }
    let colors = [
        Color {
            red: 255,
            green: 0,
            blue: 0,
        },
        Color {
            red: 0,
            green: 255,
            blue: 0,
        },
        Color {
            red: 0,
            green: 0,
            blue: 255,
        }
    ];
    for color in colors.iter() {
        println!("{}", *color);
    }
}

fn reverse<L, R>(tuple: (L, R)) -> (R, L) {
    let (left, right) = tuple;
    (right, left)
}

fn transpose(matrix: Matrix) -> Matrix {
    Matrix(matrix.0, matrix.2, matrix.1, matrix.3)
}

#[derive(Debug)]
struct Matrix(f32, f32, f32, f32);

impl std::fmt::Display for Matrix {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        writeln!(f, "({} {})", self.0, self.1);
        writeln!(f, "({} {})", self.2, self.3)
    }
}

fn tuples_p2_2() {
    let long_tuple =
        (1u8, 2u16, 3u32, 4u64, -1i8, -2i16, -3i32, -4i64, 0.1f32, 0.2f64, 'a', true);

    println!("First two elements are {} and {}", long_tuple.0, long_tuple.1);

    let tuple_of_tuple = ((1u8, 2u16, 3u32), (4u64, -1i8), -2i16);

    println!("Tuples are printable: {:?}", tuple_of_tuple);

    let pair = ('a', 1);
    println!("Pair = {:?}", pair);
    println!("Reversed Pair = {:?}", reverse(pair));

    let a_matrix = Matrix(1.1, 1.2, 2.1, 2.2);
    println!("A Matrix:\n{}", a_matrix);
    println!("Transposed:\n{}", transpose(a_matrix));
}

fn structures_p3_1() {
    struct Person<'a> {
        // 'a is a lifeline…
        // Q®: What is a lifeline?
        name: &'a str,
        age: u8,
    }

    struct Nil; // a unit struct

    struct Pair(i32, f32); // a tuple struct

    #[derive(Debug)]
    struct Point {
        // a struct with two fields
        x: f32,
        y: f32,
    }

    #[allow(dead_code)]
    #[derive(Debug)]
    struct Rectangle {
        top_left: Point,
        bottom_right: Point,
    }

    let name = "Peter";
    let age = 27;
    let _peter = Person { name, age }; // Q®: why curly braces instead of parens for record
    // instantiation syntax?

    let point = Point { x: 10.3, y: 0.4 }; // Q®: why the `:` instead of `=` for field assignment?
    println!("Point coordinates are (x={},y={})", point.x, point.y);

    let bottom_right = Point { x: 5.2, ..point };
    println!("Second Point coordinates are (x={},y={})", bottom_right.x, bottom_right.y);

    let Point { x: top_edge, y: left_edge } = point;
    let _rectangle = Rectangle {
        top_left: Point { x: left_edge, y: top_edge },
        bottom_right, // Q® How does this work? Is the field with the same name and type of this
        // variable automatically matched?
    };

    let _nil = Nil;

    let pair = Pair(1, 0.1); // Q®: aha! the named tuple uses parens. Still parens could have been
    // used for both named parameters and unnamed parameters (tuples)

    println!("Pair is {:?} and {:?}", pair.0, pair.1);

    let Pair(left, right) = pair;

    println!("left is {:?} and right is {:?}", left, right);

    fn rect_area(rectangle: Rectangle) -> f32 {
        let Rectangle { top_left, bottom_right } = rectangle;

        let height = (top_left.y - bottom_right.y).abs();
        let width = (bottom_right.x - top_left.x).abs();

        return width * height;
    }

    println!("Rectangle Area Is: {}", rect_area(_rectangle));

    fn square(bottom_left_corner: Point, side: f32) -> Rectangle {
        // TODO: side should always be positive
        Rectangle {
            top_left: Point { x: bottom_left_corner.x, y: bottom_left_corner.y + side },
            bottom_right: Point { x: bottom_left_corner.x + side, y: bottom_left_corner.y },
        }
    }

    let _bottom_left_corner = Point { x: 0.0, y: 0.0 };
    let a_square = square(_bottom_left_corner, 10.0);

    println!("Square at point {:?}", /*_bottom_left_corner,*/ a_square);
}